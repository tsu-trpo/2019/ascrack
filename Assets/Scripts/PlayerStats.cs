﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System;

public class Stat
{
    public int min, cur, max;
    public Text text;
    public string name;

    public Stat(int min, int cur, int max, Text text)
    {
        this.min = min;
        this.cur = cur;
        this.max = max;
        this.text = text;
        name = text.text;
        this.text.text = name + " " + cur + "/" + max;
    }

    public bool IsMin()
    {
        return cur == min;
    }

    public static void UpdateStat(Stat stat)
    {
        stat.text.text = System.String.Format("{0} {1}/{2}", stat.name, stat.cur, stat.max);
    }
}

public class PlayerStats : MonoBehaviour
{
    Player player;
    FirstPersonController firstPersonController;

    [SerializeField] Text HP, Food, Water, Stamina;

    Stat hp, food, water, stamina;

    int staminaRegen = 1, staminaRunDec = 250, staminaDyspneaLimit = 250;

    float staminaRegenPeriod = 0.02f;

    float foodDecPeriod = 1, waterDecPeriod = 1;

    int hungerDamage = 1, thirstDamage = 1;

    public Action<Stat> OnStatChanged;
    public Action<Stat> OnStaminaChanged;
    public Action<Stat> OnHpChanged;

    void Start()
    {
        player = GetComponent<Player>();
        firstPersonController = GetComponent<FirstPersonController>();

        hp = new Stat(0, 100, 100, HP);
        food = new Stat(0, 100, 100, Food);
        water = new Stat(0, 100, 100, Water);
        stamina = new Stat(0, 1000, 1000, Stamina);

        this.OnStatChanged += Stat.UpdateStat;
        this.OnStaminaChanged += UpdateAbilityToRun;

        InvokeRepeating("FoodDecrease", foodDecPeriod, foodDecPeriod);
        InvokeRepeating("WaterDecrease", waterDecPeriod, waterDecPeriod);
        InvokeRepeating("StaminaIncrease", staminaRegenPeriod, staminaRegenPeriod);
    }

    void UpdateAbilityToRun(Stat stat)
    {
        if (stat.cur <= stat.min)
        {
            firstPersonController.m_IsAbleToRun = false;
        }
        else
        {
            firstPersonController.m_IsAbleToRun = firstPersonController.m_IsAbleToRun || stat.cur > staminaDyspneaLimit;
        }
    }

    void ModifyStat(Stat stat, int value)
    {
        int oldStat = stat.cur;
        stat.cur = Mathf.Clamp(stat.cur + value, stat.min, stat.max);
        if (oldStat != stat.cur)
        {
            OnStatChanged(stat);
            if (stat == hp)
            {
                OnHpChanged(hp);
            }
            if (stat == stamina)
            {
                OnStaminaChanged(stamina);
            }
        }
    }

    void FoodDecrease()
    {
        ModifyStat(food, -1);
        if(food.IsMin())
        {
            ModifyStat(hp, -hungerDamage);
        }
    }

    void WaterDecrease()
    {
        ModifyStat(water, -1);
        if (water.IsMin())
        {
            ModifyStat(hp, -thirstDamage);
        }
    }

    public void StaminaDecrease()
    {
        ModifyStat(stamina, -(int)(staminaRunDec * Time.deltaTime));
    }

    public void FoodIncrease(int value)
    {
        ModifyStat(food, value);
    }

    public void WaterIncrease(int value)
    {
        ModifyStat(water, value);
    }

    void StaminaIncrease()
    {
        ModifyStat(stamina, staminaRegen);
    }
}