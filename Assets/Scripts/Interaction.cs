using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class Interaction: MonoBehaviour
{
    public static void Interact(PlayerStats playerStats){
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if(hit.collider.gameObject.TryGetComponent<Fish>(out Fish fish))
            {
                playerStats.FoodIncrease(20);
                Destroy(fish);
            }
        }
    }
}
