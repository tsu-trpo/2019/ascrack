﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prefabs : MonoBehaviour
{
    private static Prefabs instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("Duplicate");
            Destroy(this);
            return;
        }

        instance = this;
    }

    public static Prefabs Get()
    {
        return instance;
    }

}