﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
    public int hp = 100;
    const int mass = 400;
    const int fallForce = 200;
    const int disappearDelay = 20;
    Rigidbody rigid;
    
    private void Start()
    {
        EventManager.TriggerEvent(EventName.spawn_tree);
    }

    private void fallDown(Vector3 direction)
    {
        Transform transofrm = GetComponent<Transform>();
        CapsuleCollider collider = GetComponent<CapsuleCollider>();
        float height = transform.localScale.y * collider.height;
        Vector3 forcePoint = new Vector3(transform.position.x,
                              transform.position.y + height - 5,
                              transform.position.z);
        direction = direction.normalized;
        direction -= new Vector3(0, direction.y, 0);
        rigid = gameObject.AddComponent<Rigidbody>();
        rigid.mass = mass;
        rigid.AddForceAtPosition(direction * fallForce, forcePoint);
        Destroy(gameObject, disappearDelay);
    }

    void OnDestroy(){
        EventManager.TriggerEvent(EventName.destroy_tree);
    }

    private bool isFalling()
    {
        return hp <= 0;
    }

    public void recieveDamage(int damage, Vector3 direction)
    {
        if(isFalling())
        {
            return;
        }
        hp -= damage;
        if (hp < 0)
        {
            fallDown(direction);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(!isFalling())
        {
            return;
        }
        Building building = other.GetComponent<Building>();
        if(building != null)
        {
            building.crashBuilding();
        }

        if(rigid.velocity.magnitude > 1.5f)
        {
            Player player = other.GetComponent<Player>();
            if(player != null)
            {
                player.Die();
            }
        }
    }
}
