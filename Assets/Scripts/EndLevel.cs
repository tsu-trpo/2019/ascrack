﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class EndLevel : MonoBehaviour
{
    private int buildingsCount = 0;
    private int treesCount = 0;
    private bool isDead = false;
    public GameObject lostPanel;
    public GameObject wonPanel;

    void Awake()
    {
        EventManager.StartListening(EventName.spawn_building, AddBuilding);
        EventManager.StartListening(EventName.spawn_tree, AddTree);
        EventManager.StartListening(EventName.destroy_building, DelBuilding);
        EventManager.StartListening(EventName.destroy_tree, DelTree);
        EventManager.StartListening(EventName.die, Die);
    }

    void OnDestroy()
    {
        EventManager.StopListening(EventName.spawn_building, AddBuilding);
        EventManager.StopListening(EventName.spawn_tree, AddTree);
        EventManager.StopListening(EventName.destroy_building, DelBuilding);
        EventManager.StopListening(EventName.destroy_tree, DelTree);
        EventManager.StopListening(EventName.die, Die);
    }

    void Update() 
    {
        if (buildingsCount == 0){
            wonPanel.SetActive(true);
        }
        else if (treesCount == 0 || isDead){
            lostPanel.SetActive(true);
        }
    }

    void AddBuilding(){
        buildingsCount ++;
    }

    void AddTree(){
        treesCount ++;
    }

    void DelBuilding(){
        buildingsCount --;
    }

    void DelTree(){
        treesCount --;
    }
    void Die(){
        isDead = true;
    }
}