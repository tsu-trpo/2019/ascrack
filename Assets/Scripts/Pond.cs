using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Pond: MonoBehaviour
{
    public Vector3[] destinations;
    public Vector3[] fishesLocations;
    public Fish fishPrefab;

    void Start(){
        foreach (Vector3 fishLocation in fishesLocations)
        {
            Fish fish = Instantiate<Fish>(fishPrefab, fishLocation, Quaternion.identity);
            fish.Init(destinations);
        }
    }
}
