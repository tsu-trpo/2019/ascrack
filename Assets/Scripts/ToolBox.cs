using UnityEngine;

class ToolBox : MonoBehaviour
{
    public Tool[] tools;
    Tool currentTool_;
    Tool currentTool
    {
        get
        {
            return currentTool_;
        }

        set
        {
            if (currentTool_)
            {
                currentTool_.gameObject.SetActive(false);
            }
            currentTool_ = value;
            currentTool_.gameObject.SetActive(true);
        }
    }
    int currentIndex = 0;

    void Start()
    {
        if (tools.Length != 0)
        {
            currentTool = tools[0];
        }
    }

    void SwitchTool(int offset=1)
    {
        currentIndex = (tools.Length + currentIndex + offset) % tools.Length;
        currentTool = tools[currentIndex];
    }

    public void NextTool()
    {
        SwitchTool();
    }

    public void PrevTool()
    {
        SwitchTool(-1);
    }

    public void UseTool()
    {
        currentTool?.Use();
    }
}
