public class Buttons
{
    public const string use = "Fire1";
    public const string interact = "f";
    public const string creationMode = "CreationMode";
    public const string toolSwitchAxis = "Mouse ScrollWheel";
    public const string altNextTool = "NextTool";
    public const string altPrevTool = "PrevTool";
}
