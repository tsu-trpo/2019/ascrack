using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Fish: MonoBehaviour
{
    private bool isMoving = false;
    private float fishSpeed = 5.0f;
    private Vector3 currentDestination;
    private Vector3[] destinations;

    public void Init(Vector3[] destinations)
    {
        this.destinations = destinations;
    }

    void Update(){
        if(isMoving == false){
            currentDestination = destinations[Random.Range(0, destinations.Length)];
            isMoving = true;
        }
        transform.LookAt(currentDestination);
        transform.position = Vector3.MoveTowards(transform.position, currentDestination, fishSpeed * Time.deltaTime);
        if(transform.position == currentDestination){
            isMoving = false;
        }
    }
}
