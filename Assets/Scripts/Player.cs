﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


[RequireComponent(typeof(ToolBox))]
public class Player : MonoBehaviour
{
    public GameObject builder;
    public AudioClip dieSound;

    ToolBox toolBox;
    AudioSource soundSource;
    CharacterController controller;
    FirstPersonController firstPersonController;
    PlayerStats playerStats;
    Inventory inventory;

    void Start()
    {
        controller = GetComponent<CharacterController>();
        firstPersonController = GetComponent<FirstPersonController>();
        toolBox = GetComponent<ToolBox>();
        soundSource = GetComponent<AudioSource>();
        soundSource.playOnAwake = false;
        soundSource.loop = false;

        playerStats = GetComponent<PlayerStats>();
        playerStats.OnHpChanged += DamageReceived;

    }

    void Update()
    {
        if (Input.GetButtonDown(Buttons.creationMode))
        {
            builder.SetActive(!builder.activeSelf);
        }
        if (Input.GetButtonDown(Buttons.use))
        {
            toolBox.UseTool();
        }
        if (Input.GetAxis(Buttons.toolSwitchAxis) > 0 || Input.GetButtonDown(Buttons.altNextTool))
        {
            toolBox.NextTool();
        }
        if (Input.GetAxis(Buttons.toolSwitchAxis) < 0 || Input.GetButtonDown(Buttons.altPrevTool))
        {
            toolBox.PrevTool();
        }

        if (Input.GetKeyDown(Buttons.interact))
        {
            Interaction.Interact(playerStats);
        }
    }

    public void DamageReceived(Stat stat)
    {
        if(stat.IsMin())
        {
            Die();
        }
    }

    public void Die()
    {
        playerStats.OnHpChanged -= DamageReceived;
        this.enabled = false;
        firstPersonController.enabled = false;
        Camera.main.transform.Rotate(new Vector3(0, 90, -90));
        controller.enabled = false;
        soundSource.clip = dieSound;
        soundSource.Play();
        EventManager.TriggerEvent(EventName.die);
    }
}
