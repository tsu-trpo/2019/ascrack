using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum EventName {
    spawn_building,
    spawn_tree,
    destroy_building,
    destroy_tree,
    die
}

public class EventManager : MonoBehaviour {
    
    private Dictionary<EventName, UnityEvent> eventDictionary = new Dictionary<EventName, UnityEvent>();
    
    private static EventManager eventManager;
    
    public static EventManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("Duplicate");
            Destroy(this);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);
    }

    public static EventManager Get()
    {
        return instance;
    }
    
    public static void StartListening(EventName eventName, UnityAction listener) {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.AddListener(listener);
        }
        else {
            thisEvent = new UnityEvent();
            thisEvent.AddListener(listener);
            instance.eventDictionary.Add(eventName, thisEvent);
        }
    }
    
    public static void StopListening (EventName eventName, UnityAction listener) {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.RemoveListener(listener);
        }
    }
    
    public static void TriggerEvent (EventName eventName) {
        UnityEvent thisEvent = null;
        if (instance.eventDictionary.TryGetValue(eventName, out thisEvent)) {
            thisEvent.Invoke();
        }
    }

    public static IEnumerator TriggerEventWithDelay(EventName eventname, float delay) {
        yield return new WaitForSeconds(delay);
        TriggerEvent(eventname);
    }
}
