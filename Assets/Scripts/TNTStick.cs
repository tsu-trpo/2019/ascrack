using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class TNTStick : MonoBehaviour
{
    public GameObject explosion;

    const int explodeDelay = 3;
    AudioSource soundSource;

    void Start()
    {
        soundSource = GetComponent<AudioSource>();
        soundSource.playOnAwake = false;
        soundSource.loop = false;
        Invoke("Explode", explodeDelay);
    }

    void Explode()
    {
        explosion.SetActive(true);
        soundSource.Play();
        Destroy(gameObject, 5.0f);
    }

    void OnTriggerEnter(Collider other)
    {
        Building building = other.GetComponent<Building>();
        if(building != null)
        {
            building.crashBuilding();
        }
    }
}
