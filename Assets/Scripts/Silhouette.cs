﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Silhouette : MonoBehaviour
{
    bool isCollided = false;

    void OnTriggerEnter(Collider other)
    {
        if (ShouldBeIgnored(other))
        {
            return;
        }
        isCollided = true;
    }

    void OnTriggerStay(Collider other)
    {
        if (ShouldBeIgnored(other))
        {
            return;
        }
        isCollided = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (ShouldBeIgnored(other))
        {
            return;
        }
        isCollided = false;
    }

    public bool IsCollided()
    {
        return isCollided;
    }

    bool ShouldBeIgnored(Collider other)
    {
        return (other.gameObject.layer == Layers.Surface);
    } 
}
