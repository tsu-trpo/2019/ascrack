﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LayersStr
{
    public static string Surface = "Surface";
}

public static class Layers
{
    public static int Surface = LayerMask.NameToLayer(LayersStr.Surface);
}

public static class LayersMasks
{
    public static int Surface = LayerMask.GetMask(LayersStr.Surface);
}

