﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public GameObject wreckage;
    
    private void Start()
    {
        EventManager.TriggerEvent(EventName.spawn_building);
    }
    
    public void crashBuilding()
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        wreckage.SetActive(true);
        Destroy(gameObject,10.0f);
    }

    void OnDestroy(){
        EventManager.TriggerEvent(EventName.destroy_building);
    }
}
