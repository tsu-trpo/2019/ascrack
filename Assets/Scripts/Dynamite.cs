using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Dynamite : Tool
{
    public AudioClip igniteSound;
    public AudioClip burningSound;
    public AudioClip throwSound;
    public ParticleSystem lighterPS;
    public ParticleSystem dynamitePS;
    public GameObject dynamitePrefab;
    public int dynamiteCount = 5;

    const int throwDelay = 2;
    const int soundDelay = 1;
    const int throwForce = 800;
    readonly int hitHash = Animator.StringToHash("Ignite");
    Animator anim;
    AudioSource soundSource;

    void Start()
    {
        anim = GetComponent<Animator>();
        soundSource = GetComponent<AudioSource>();
        soundSource.playOnAwake = false;
        soundSource.loop = false;
    }

    public override void Use()
    {
        if (IsAnimPlaying())
        {
            return;
        }
        soundSource.clip = igniteSound;
        anim.SetTrigger(hitHash);
        soundSource.PlayDelayed(soundDelay);
        lighterPS.Play();
        dynamitePS.Play();
        Invoke("Throw", throwDelay);
    }

    private void Throw()
    {
        Vector3 pos = new Vector3(transform.position.x,
                                  transform.position.y + 1,
                                  transform.position.z);
        GameObject dynamite = Instantiate(dynamitePrefab, pos, transform.rotation);
        dynamite.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * throwForce);
    }

    bool IsAnimPlaying()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("Base.Ignite");
    }
}
