﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Builder : MonoBehaviour
{
    public GameObject silhouettePrefab;
    public GameObject buildingPrefab;
    Silhouette silhouette;
    float creationDistance = 25.0f;

    void Start()
    {
        silhouette = Instantiate(silhouettePrefab,transform).AddComponent<Silhouette>();
    }

    void Update()
    {
        UpdatePosition();
        if (Input.GetButtonDown(Buttons.use))
        {
            TryBuild();
        }
    }

    void UpdatePosition()
    {
        var silhouetteGo = silhouette.gameObject;
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, creationDistance, LayersMasks.Surface))
        {
            silhouette.transform.position = hit.point;
            silhouetteGo.SetActive(true);
        }
        else
        {
            silhouetteGo.SetActive(false);
        }
    }

    void TryBuild()
    {
        if ( !silhouette.IsCollided() && silhouette.gameObject.activeSelf )
        {
            Instantiate(buildingPrefab, silhouette.transform.position, silhouette.transform.rotation);
        }
    }
}
