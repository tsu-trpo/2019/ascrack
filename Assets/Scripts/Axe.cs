﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Axe : Tool
{
    public string hitAnimName;
    public AudioClip missSound;
    public AudioClip hitSound;

    int damage = 30;
    readonly int hitHash = Animator.StringToHash("Hit");
    readonly string returnTrigger = "Return";
    readonly string hitAnimationMultiplier = "Multiplier";
    Animator anim;
    AudioSource soundSource;
    float soundDelay = 0.3f;
    Collider axeCollider;

    void Start()
    {
        anim = GetComponent<Animator>();
        soundSource = GetComponent<AudioSource>();
        soundSource.playOnAwake = false;
        soundSource.loop = false;
        axeCollider = GetComponent<Collider>();
    }

    public override void Use()
    {
        if (IsAnimPlaying())
        {
            return;
        }
        axeCollider.isTrigger = true;
        anim.SetFloat(hitAnimationMultiplier, 1);
        anim.SetTrigger(hitHash);
        soundSource.clip = missSound;
        soundSource.PlayDelayed(soundDelay);
    }

    bool IsAnimPlaying()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("Base.Hit");
    }

    void OnTriggerEnter(Collider collider)
    {
        if (!IsAnimPlaying())
        {
            return;
        }
        soundSource.clip = hitSound;
        soundSource.Play();

        axeCollider.isTrigger = false;

        anim.SetFloat(hitAnimationMultiplier, -1);
        anim.SetTrigger(returnTrigger);

        Tree tree = collider.GetComponent<Tree>();
        tree.recieveDamage(damage, transform.forward);
    }
}
